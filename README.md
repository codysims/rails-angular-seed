# rails-angular-seed

```
bundle install
rake db:create db:migrate
rake bower:install
```

Rails tests:

```
bin/rspec
```

JS unit tests:

```
npm test
```

E2E tests:

```
npm start
npm run protractor
```
