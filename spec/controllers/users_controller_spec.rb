require 'rails_helper'

describe UsersController do
  let(:user) { FactoryGirl.create(:user) }

  describe 'GET /current' do
    before do
      sign_in user
    end

    it 'returns the current user' do
      get :current, format: :json
      expect(response.body).to eq(UserSerializer.new(user).to_json)
    end
  end
end
