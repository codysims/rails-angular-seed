require 'rails_helper'

describe AppController do
  before do
    sign_in FactoryGirl.create(:user)
  end

  describe 'GET #index' do
    it 'responds successfully with app page' do
      get :index
      expect(response).to render_template('app/index')
    end
  end
end