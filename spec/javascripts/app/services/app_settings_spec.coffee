describe 'appSettings', ->
  beforeEach module 'app'

  describe '#fetch', ->
    describe 'first call', ->
      it 'calls to the settings endpoint, returns a promise', inject ($httpBackend, appSettings) ->
        $httpBackend.expectGET('/app/settings').respond({ date: '2014-07-27' })
        settingsPromise = appSettings.fetch()
        settingsPromise.then((response) ->
          expect(response.data.date).toEqual(new Date('2014-07-27'))
        )
        $httpBackend.flush()

    describe 'subsequent calls', ->
      it 'returns the already defined promise', inject ($httpBackend, appSettings) ->
        $httpBackend.expectGET('/app/settings').respond({ date: '2014-07-27' })
        settingsPromise1 = appSettings.fetch()
        settingsPromise2 = appSettings.fetch()
        expect(settingsPromise1).toBe(settingsPromise2)

  describe '#get', ->
    describe 'prior to promise being resolved', ->
      it 'returns undefined', inject (appSettings) ->
        expect(appSettings.get()).toBeUndefined()

    describe 'after promise is resolved', ->
      it 'returns settings data', inject ($httpBackend, appSettings) ->
        $httpBackend.expectGET('/app/settings').respond({ date: '2014-07-27' })
        settingsPromise = appSettings.fetch()
        settingsPromise.then((response) ->
          expect(response.data.date).toEqual(new Date('2014-07-27'))
        )
        $httpBackend.flush()
        expect(appSettings.get()).toEqual({ date: new Date('2014-07-27') })