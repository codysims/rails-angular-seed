Rollbar = { error: -> }

describe 'appError', ->
  beforeEach module 'app'

  describe '#boom', ->
    it 'sends error to Rollbar', inject (appError) ->
      spyOn(Rollbar, 'error')
      appError.boom('message', {})
      expect(Rollbar.error).toHaveBeenCalled()

    it 'opens boom modal', inject (appError, $modal) ->
      spyOn($modal, 'open')
      appError.boom('message', {})
      expect($modal.open).toHaveBeenCalled()