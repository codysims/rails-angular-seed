describe 'User', ->
  beforeEach module 'app'

  describe '.fetchCurrent', ->
    it 'makes a request & handles the response', inject ($httpBackend, User) ->
      $httpBackend.expectGET('/users/current.json').respond(200, {id: 42})
      currentUser = undefined
      User.fetchCurrent().then (user) ->
        currentUser = user
      $httpBackend.flush()
      expect(currentUser.id).toEqual(42)
      expect(User.getCurrent()).toEqual(currentUser)
