describe 'HomeController', ->
  beforeEach module 'app'
  beforeEach module 'app.templates'
  beforeEach inject ($rootScope, $controller) ->
    $controller('HomeController', { '$scope': $rootScope.$new(), 'User': User })