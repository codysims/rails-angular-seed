describe 'AppController', ->
  createController = undefined

  beforeEach module 'app'
  beforeEach module 'app.templates'
  beforeEach inject ($rootScope, $controller, User) ->
    createController = ->
      $controller('AppController', { '$scope': $rootScope.$new(), 'User': User })

  describe 'with no errors getting current user', ->
    it 'gets the current user', inject ($httpBackend, User) ->
      $httpBackend.expectGET('/users/current.json').respond(200, { id: 42 })
      createController()
      $httpBackend.flush()
      expect(User.getCurrent()).toBeDefined()
      expect(User.getCurrent().id).toEqual(42)

    describe 'when user has no enabled TwitterUsers', ->
      it 'redirects to account settings', inject ($httpBackend, $location) ->
        $httpBackend.expectGET('/users/current.json').respond(200, { id: 42, twitter_users: [{ id: 1, state: 'disabled' }] })
        createController()
        $httpBackend.flush()
        expect($location.path()).toEqual('/settings')

    describe 'when user has enabled TwitterUsers', ->
      it 'does not redirect', inject ($httpBackend, $location) ->
        $httpBackend.expectGET('/users/current.json').respond(200, { id: 42, twitter_users: [{ id: 1, state: 'enabled' }] })
        createController()
        path = $location.path()
        $httpBackend.flush()
        expect($location.path()).toEqual(path)