if (!String.prototype.endsWith){
  String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
  };
};

exports.config = {
  allScriptsTimeout: 11000,
  specs: [
    'e2e/*.coffee'
  ],
  capabilities: {
    'browserName': 'chrome'
  },
  baseUrl: 'http://localhost:8000/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    defaultTimeoutInterval: 30000
  }
//  ,
//  onPrepare: function() {
//    browser.ignoreSynchronization = true;
//    browser.driver.get('http://localhost:8000');
//    email_input = browser.driver.findElement(By.id('user_email'));
//    password_input = browser.driver.findElement(By.id('user_password'));
//    submit_button = browser.driver.findElement(By.css('input[type=submit]'));
//    email_input.sendKeys('user@example.com');
//    password_input.sendKeys('abcd1234');
//    submit_button.click();
//    browser.ignoreSynchronization = false;
//  }
};