describe 'home page', ->
  it 'loads successfully', ->
    browser.driver.get('http://localhost:8000/')
    buttonElement = browser.driver.findElement(By.css('.btn'))
    expect(buttonElement.getText()).toBe('Sign In')
