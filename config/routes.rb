RailsAngularSeed::Application.routes.draw do
  devise_for :users

  authenticated do
    get 'users/current'
    get 'users/verify/:token', to: 'users#verify'
    get 'app/settings'

    root 'app#index', as: :authenticated_root
  end

  root 'home#index'
end
