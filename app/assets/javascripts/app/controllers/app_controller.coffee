@app.controller 'AppController', ($scope, $location, User, appError) ->
  User.fetchCurrent().then ((currentUser) ->
    unless currentUser.enabledTwitterUserCount() > 0
      $location.path '/settings'
  )