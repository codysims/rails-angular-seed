@app.factory 'appError', ($modal) ->
  {
    boom: (message, payload) ->
      Rollbar.error "appError.boom - #{message}", payload
      $modal.open
        templateUrl: 'modals/app_error_boom.html'
        keyboard: false
  }
