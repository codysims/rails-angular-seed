@app.factory 'appSettings', ($http) ->
  settings = settingsPromise = undefined

  {
    fetch: ->
      if angular.isDefined(settingsPromise)
        settingsPromise
      else
        settingsPromise = $http({ method: 'GET', url: '/app/settings' })
        settingsPromise.then((response) ->
          settings = response.data
          if angular.isDefined(settings.date)
            settings.date = new Date(settings.date)
        )
        settingsPromise

    get: -> settings
  }
