@app.factory 'User', (ActiveRecord) ->
  current = currentPromise = undefined

  UserAR = ActiveRecord.extend
    $urlRoot: '/users'
    enabledTwitterUserCount: ->
      found = 0
      angular.forEach @twitter_users, (twitter_user) ->
        found = found + 1 if twitter_user.state == 'enabled'
      found

  UserAR.fetchCurrent = ->
    if angular.isDefined(currentPromise)
      currentPromise
    else
      model = new this()
      currentPromise = model.$fetch(url: '/users/current.json')
      currentPromise.then((response) -> current = response)
      currentPromise

  UserAR.getCurrent = -> current

  UserAR
