#= require angular/angular
#= require angular-resource/angular-resource
#= require angular-route/angular-route
#= require angular-ui-bootstrap-bower/ui-bootstrap
#= require angular-ui-bootstrap-bower/ui-bootstrap-tpls
#= require angular-activerecord/src/angular-activerecord

#= require_tree ./app

@app = angular.module('app', ['ngRoute', 'ngResource', 'ui.bootstrap', 'ActiveRecord'])

@app.config ($routeProvider) ->
  $routeProvider
  .when('/', {
      templateUrl: 'home.html'
      controller: 'HomeController'
      resolve: {
        currentUserPromise: ['User', (User) -> User.fetchCurrent()]
        settingsPromise: ['appSettings', (appSettings) -> appSettings.fetch()]
      }
    })
  .otherwise({ redirectTo: '/' })

