module AppHelper
  def angular_templates
    script_tags = ''
    template_pathname = Pathname.new(Rails.root.join('app', 'assets', 'javascripts', 'app', 'templates'))
    template_pathname_part_count = template_pathname.each_filename.count

    Dir[File.join(template_pathname, '**', '*')].each do |file_path|
      pathname = Pathname.new(file_path)
      pathname_part_count = pathname.each_filename.count
      next if pathname.directory?

      template_id_parts = pathname.each_filename.to_a.last(pathname_part_count - template_pathname_part_count)
      template_id_parts.last.sub!(/\.haml$/, '.html')
      template_id = template_id_parts.join('/')

      script_tags += content_tag(:script, render(file: pathname), id: template_id, type: 'text/ng-template')
    end

    script_tags.html_safe
  end
end